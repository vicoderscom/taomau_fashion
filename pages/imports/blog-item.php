<div class="col-sm-6 blog-news">
	<div class="news-box-small clearfix">
		<a class="block pull-left box-image" href="<?php echo site_url('pages/blog-show.php');?>" style="background-image: url('../assets/images/blog/box-small.png');"></a>
		<div class="pull-right box-description">
			<div class="content-wrapper">
				<h3 class="content-title">
					<a class="inline-block" href="<?php echo site_url('/pages/blog-show.php');?>">làm sao để kết hợp những phụ kiện thêm nổi bật?</a>
				</h3>
				<p class="content-date">26-06-2017</p>
				<p class="content-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
			</div>
		</div>
	</div>
</div>