<?php require '../header.php';?>
<div class="detail-product-wrapper">
    <div class="banner-section" style="background-image: url('../assets/images/list/banner.png');">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href=" " title=" ">Trang chủ</a> / <a class="active" href="" title="">Sweater</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="slidebar-section col-md-2">
                <div class="items">
                    <img src="../assets/images/detail/maskmini.png" alt="" style="background-image: url('../assets/images/detail/mini_img.png');">
                </div>
                <div class="items">
                    <img src="../assets/images/detail/maskmini.png" alt="" style="background-image: url('../assets/images/detail/mini_img.png');">
                </div>
                <div class="items">
                    <img src="../assets/images/detail/maskmini.png" alt="" style="background-image: url('../assets/images/detail/mini_img.png');">
                </div>
            </div>
            <div class="main-image-section col-md-6">
                <div class="items">
                    <img class="img-responsive" src="../assets/images/detail/maskbig.png" alt="" style="background-image: url('../assets/images/detail/big.png');">
                </div>
            </div>
            <div class="detail-section col-md-4 clearfix">
                <form>
                    <h1>Sweater jonafine</h1>
                    <p>Thương hiệu : <span>Laura</span></p>
                    <p>Loại sản phẩm : <span>Áo Sweater</span></p>
                    <p>Trạng thái : <span>Còn hàng</span></p>
                    <hr>
                    <p class="price">Giá : <span>300.000 VNĐ</span></p>
                    <p>Số lượng :
                        <input class="number" min="1" max="10" type="number" name="" value="1" placeholder="">
                    </p>
                    <p class="size">Size:
                        <span size="size-m">M</span>
                        <span size="size-l">L</span>
                        <span size="size-xl">XL</span>
                        <input type="radio" name="size" class="size-m" value="M" hidden>
                        <input type="radio" name="size" class="size-l" value="L" hidden>
                        <input type="radio" name="size" class="size-xl" value="XL" hidden>
                    </p>
                    <img src="../assets/images/detail/xh.png" alt="">
                    <a href="#" class="pay" title="">MUA NGAY</a>
                    <a href="#" class="add" title="">THÊM GIỎ HÀNG</a>
                </form>
            </div>
            <div class="content-section col-md-12 clearfix">
                <ul class="nav nav-tabs" role="tablist">
                    <li  class="nav-item">
                        <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Mô tả</a>
                    </li>|
                    <li  class="nav-item">
                        <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Đánh giá</a>
                    </li>|
                    <li  class="nav-item">
                        <a class="nav-link" href="#references" role="tab" data-toggle="tab">Shipping & Delivery</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="profile">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officiore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillumdo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officiore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillumdo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="buzz">
                        <p>Đánh giá</p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="references">
                        <p>Shipping & Delivery</p>
                    </div>
                </div>
            </div>
            <div class="related-products-section col-md-12">
                <h1 class="mar-0">Sản phẩm liên quan</h1>
                <?php for ($i = 0; $i < 4; $i++) {?>
                <div class="product col-xs-12 col-sm-6 col-md-3">
                    <?php require 'product.php';?>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<?php require '../footer.php';