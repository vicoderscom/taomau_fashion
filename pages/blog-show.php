<?php require '../header.php';?>
<div class="blog-show-page">
	<div class="banner" style="background-image: url('../assets/images/blog/blog-show.png');">
		<div class="container banner-title-wrapper">
			<div class="row banner-title-wrapper">
				<div class="col-sm-11 col-sm-offset-1 banner-title-wrapper flex-box flex-center">
					<h1 class="banner-title">Trang chủ / Blog / <span class="current-page">Drag and drop page builder</span></h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 blog-wrapper">
				<h1 class="blog-title">Drag and drop page builder</h1>

				<div class="blog-info-wrapper clearfix">
					<div class="col-sm-6 blog-info">
						<p class="date"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Ngày 26-06-2017</p>
					</div>
					<div class="col-sm-6 blog-info">
						<p class="author">Admin: Alex bristh</p>
					</div>
				</div>

				<div class="blog-content">
					<p>Suspendisse ac quam sed massa tincidunt blandit. Cras aliquam mi sit amet justo rutrum, at dignissim massa gravida. Donec eu libero aliquet, porttitor lacus elementum, sagittis dui. Pellentesque lacus lacus, efficitur ut rutrum vel, feugiat sit amet dui. Ut sed libero luctus, molestie augue et, vehicula odio. Phasellus feugiat risus mauris, in accumsan ipsum mollis vel. Nulla cursus dui ut ante volutpat, quis ultrices velit elementum. Vivamus ullamcorper velit a pretium finibus.</p>
					
					<p>Nulla sed mi leo, sit amet molestie nulla. Phasellus lobortis blandit ipsum, at adipiscing ero It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
					
					<img src="../assets/images/blog/blog-show.png">
					
					<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
					
					<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container title-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="title">Bài viết liên quan</h1>
			</div>
		</div>
	</div>
	
	<div class="container blog-news-wrapper">
		<div class="row">
			<?php
          for ($i = 0; $i < 6; $i++) {
              require './imports/blog-item.php';
          }
      ?>
		</div>
	</div>
</div>
<?php require '../footer.php';?>