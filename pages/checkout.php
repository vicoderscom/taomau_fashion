<?php require '../header.php';?>
<div class="checkout-page">
	<div class="banner" style="background-image: url('../assets/images/contact/contact-banner.png');">
		<div class="container banner-title-wrapper">
			<div class="row banner-title-wrapper">
				<div class="col-sm-11 col-sm-offset-1 banner-title-wrapper flex-box flex-center">
					<h1 class="banner-title">Trang chủ / <span class="current-page">Checkout</span></h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="checkout-page-title">REVIEW YOUR ORDER</h1>
			</div>
		</div>
	</div>
	
	<div class="container order-info-wrapper">
		<div class="row">
			<div class="col-md-5 col-lg-4 order-wrapper">
				<div class="order-item order-main-info">
					<?php for ($i = 0; $i < 2; $i++) {?>
					<div class="order-product-wrapper clearfix">
						<div class="pull-left order-product-image-container">
							<div class="order-product-image-wrapper">
								<img class="block order-product-image" src="../assets/images/checkout/product-mask.png" style="background-image: url('../assets/images/checkout/product.png');">
							</div>
						</div>
						
						<div class="pull-left flex-box flex-end product-info-wrapper">
							<div class="content-wrapper">
								<p class="product-info">Hoodie WBlack</p>
								<p class="product-info">L</p>
							</div>
						</div>
						<div class="pull-right flex-box flex-end product-price-wrapper">
							<p class="product-price">300.000 VNĐ</p>
						</div>
					</div>
					<?php }?>
				</div>
				<div class="order-item order-plus-info clearfix">
					<div class="pull-left name">Vận chuyển</div>
					<div class="pull-right price">20.000 VNĐ</div>
				</div>
				<div class="order-total clearfix">
					<span class="pull-left name">Toàn bộ</span>
					<span class="pull-right price">620.000 VNĐ</span>
				</div>
			</div>
			
			<div class="col-md-7 col-lg-7 col-lg-offset-1 customer-info-wrapper">
				<h3 class="form-header customer-info-title">Thông tin khách hàng</h3>
				<form class="clearfix">
					<div class="col-sm-6">
						<input class="form-input" type="text" name="name" placeholder="Họ và tên">
					</div>
					<div class="col-sm-6">
						<input class="form-input" type="text" name="address" placeholder="Số điện thoại">
					</div>
					<div class="col-xs-12">
						<input class="form-input" type="email" name="email" placeholder="Email">
					</div>
					<div class="col-sm-4">
						<input class="form-input" type="text" name="phone" placeholder="Số nhà tên đường">
					</div>
					<div class="col-sm-4">
						<input class="form-input" type="text" name="phone" placeholder="Thành phố">
					</div>
					<div class="col-sm-4">
						<input class="form-input" type="text" name="phone" placeholder="Mã bưu điện">
					</div>
					<div class="col-xs-12">
						<textarea class="form-input" name="content" placeholder="Ghi chú"></textarea>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div class="container payment-wrapper">
		<div class="row">
			<h3 class="form-header payment-title">Phương thức thanh toán</h3>
			<form>
				<div class="clearfix radio-container">
					<div class="col-sm-4 clearfix">
						<div class="payment-atm">
							<label class="block payment-input left-item" for="atm">Thẻ ATM, Online banking</label>
							<div class="payment-input left-item">
								<input type="radio" name="payment" id="atm">
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div>
							<label class="block payment-input center-item" for="credit">Thẻ tín dụng, ghi nợ quốc tế</label>
							<div class="payment-input center-item">
								<input type="radio" name="payment" id="credit">
							</div>
						</div>
					</div>
					<div class="col-sm-4 clearfix">
						<div class="payment-cash">
							<label class="block payment-input right-item" for="cash">Tiền mặt, chuyển khoản</label>
							<div class="payment-input right-item">
								<input type="radio" name="payment" id="cash">
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 submit-btn-wrapper">
					<button class="submit-btn" type="submit">GỬI ĐƠN HÀNG</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php require '../footer.php';?>