<?php require '../header.php';?>
<div class="listproducts_wrapper">
    <div class="banner-section" style="background-image: url('../assets/images/list/banner.png');">
		 <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href=" " title=" ">Trang chủ</a> / <a class="active" href="" title="">Shop</a>
                    </p>
                </div>
            </div>
        </div>
	</div>

	<div class="main-section ">
		<div class="container ">
			<div class="row ">
				<div class="menu col-md-3 ">
					<h1 class="mar-0 ">Sản phẩm nổi bật</h1>
					<div class="menu-main ">
						<p>
						  <label for="amount">Giá:</label>
						  <input type="text" id="amount" readonly >
						</p>
						<div id="slider-range"></div>
						<h2>Thương hiệu</h2>
						<ul class="mar-0 " >
							<li><a href=" " title=" ">Adidas</a></li>
							<li><a href=" " title=" ">Combi</a></li>
							<li><a href=" " title=" ">Fisher Price</a></li>
							<li><a href=" " title=" ">Sassy</a></li>
							<li><a href=" " title=" ">Other brands</a></li>
							<li><a href=" " title=" ">Nike </a></li>
							<li><a href=" " title=" ">Tommy</a></li>
							<li><a href=" " title=" ">Mightyrigz</a></li>
						</ul>
					</div>
				</div>
				<div class="products col-md-9 pad-0 ">
					<div class="categories col-md-12 pad-0 ">
 						<label for=" ">Tìm kiếm</label>
 						<select name=" ">
 							<option value=" ">Áo jacket</option>
 							<option value=" ">Áo jacket 1</option>
 							<option value=" ">Áo jacket 2</option>
 						</select>
					</div>
					<div class="list-products ">
						<?php for ($i = 0; $i < 9; $i++) {?>
						<div class="product col-sm-6 col-md-4 ">
							<?php require 'product.php';?>
						</div>
						<?php }?>
 					</div>
 					<div class="paginate col-xs-12 col-md-12 col-md-12 ">
						<ul>
							<li><a class="fa fa-long-arrow-left " href=" " title=" "></a></li>
							<li><a class="active " href=" " title=" ">1</a></li>
							<li><a href=" " title=" ">2</a></li>
							<li><a href=" " title=" ">3</a></li>
							<li><a href=" " title=" ">...</a></li>
							<li><a href=" " title=" ">32</a></li>
							<li><a class="fa fa-long-arrow-right " href=" " title=" "></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="register">
				<div class="col-lg-6 col-md-6">
					<div class="register_mail">
						<p class="p1">Đăng ký Email</p>
						<p>Những tin tức, phiếu mua hàng và thông tin mới nhất</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form_mail">
						<input type="text" name="" placeholder="Nhập email của bạn">
						<button><i class="fa fa-envelope" aria-hidden="true"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require '../footer.php';?>