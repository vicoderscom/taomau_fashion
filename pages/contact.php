<?php require '../header.php';?>
<div class="contact-page">
	<div class="banner" style="background-image: url('../assets/images/contact/contact-banner.png');">
		<div class="container banner-title-wrapper">
			<div class="row banner-title-wrapper">
				<div class="col-sm-11 col-sm-offset-1 banner-title-wrapper flex-box flex-center">
					<h1 class="banner-title">Trang chủ / <span class="current-page">Liên hệ</span></h1>
				</div>
			</div>
		</div>
	</div>
	
	<iframe class="maps" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14897.681787666377!2d105.82317029999999!3d21.01585645!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1519268703854" width="100%" height="352px" frameborder="0" style="border:0" allowfullscreen></iframe>
	
	<div class="contact-form-wrapper" style="background-image: url('../assets/images/contact/background.png');">
		<div class="container">
			<div class="row">
				<div class="col-md-7 contact-form">
					<p class="form-title">Công ty cổ phần dịch vụ và truyền thông đa phương tiện Việt Nam</p>
					<p>Địa chỉ: Phòng 107, Tòa nhà CFM , 23 Láng Hạ, Ba Đình, Hà Nội</p>
					<p>SĐT: (+84) 985136895</p>
					<p class="separator">Email: vicoders@gmail.com</p>
					<form>
						<input class="form-input" type="text" name="name" placeholder="Name">
						<input class="form-input" type="text" name="address" placeholder="Address">
						<input class="form-input" type="email" name="email" placeholder="Email">
						<input class="form-input" type="text" name="phone" placeholder="Phone">
						<textarea class="form-input" name="content" placeholder="Content"></textarea>
						<div class="submit-btn-wrapper">
							<button class="submit-btn" type="submit">SUBMIT</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require '../footer.php';?>