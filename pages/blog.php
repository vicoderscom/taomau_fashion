<?php require '../header.php';?>
<div class="blog-page">
	<div class="banner" style="background-image: url('../assets/images/blog/blog-banner.png');">
		<div class="container banner-title-wrapper">
			<div class="row banner-title-wrapper">
				<div class="col-sm-11 col-sm-offset-1 banner-title-wrapper flex-box flex-center">
					<h1 class="banner-title">Trang chủ / <span class="current-page">Blog</span></h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container title-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="title">Tin nổi bật</h1>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row blog-top-slider">
			<?php for ($i = 0; $i < 3; $i++) {?>
			<div class="col-sm-6 news-feature">
				<div class="news-box-large">
					<a class="block" href="<?php echo site_url('pages/blog-show.php'); ?>">
						<img class="img-responsive box-image" src="../assets/images/blog/box-large-mask.png" style="background-image: url('../assets/images/blog/box-large.png');">
					</a>
					<div class="box-description">
						<div class="content-wrapper">
							<p class="content-date"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Ngày 26-06-2017</p>
							<h3 class="content-title">
								<a class="inline-block" href="<?php echo site_url('pages/blog-show.php'); ?>">Drag and drop page builder</a>
							</h3>
							<p class="content-description">Suspendisse ac quam sed massa tincidunt blandit. Cras aliquam mi sit amet justo rutrum, at dignissim massa gravida. Donec eu libero aliquet, porttitor lacus elementum, sagittis dui. Pellentesque lacus lacus, efficitur ut rutrum vel, feugiat sit amet dui. Ut sed libero luctus, molestie augue et, vehicula odio. Phasellus feugiat risus mauris, in accumsan</p>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
		</div>
	</div>

	<div class="container title-wrapper">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="title">Fashion blog</h1>
			</div>
		</div>
	</div>

	<div class="container blog-news-wrapper">
		<div class="row">
			<?php
          for ($i = 0; $i < 6; $i++) {
              require './imports/blog-item.php';
          }
      ?>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="paginate col-xs-12">
				<ul>
					<li><a class="fa fa-long-arrow-left" href="" title=""></a></li>
					<li><a class="active" href="" title="">1</a></li>
					<li><a href="" title="">2</a></li>
					<li><a href="" title="">3</a></li>
					<li><a class="fa fa-long-arrow-right" href="" title=""></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php require '../footer.php';?>