<?php require '../header.php';?>
<div class="cart_wrapper">
    <div class="banner-section" style="background-image: url('../assets/images/list/banner.png');">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href=" " title=" ">Trang chủ</a> / <a class="active" href="" title="">Giỏ hàng của bạn</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="cart-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <ul>
                            <li> </li>
                            <li> </li>
                            <li>SẢN PHẨM</li>
                            <li>SỐ LƯỢNG</li>
                            <li>ĐƠN GIÁ</li>
                            <li>THÀNH TIỀN</li>
                        </ul>
                    </div>
                    <div class="item">
                        <ul>
                            <li><a href="" title=""><i class="fa fa-times"></i></a></li>
                            <li><img width="121" height="162" src="../assets/images/cart/sp.png" alt=""></li>
                            <li>
                                <p><span class="child-title">SẢN PHẨM:</span> HOODIE WBLACK
                                </p>
                                <p class="size">SIZE: M</p>
                            </li>
                            <li class="quanity">
                                <span class="child-title">SỐ LƯỢNG:</span>
                                <div class="number-spinner">
                                    <div class="box">
                                        <span class="input-group-btn reduce">
                                            <button data-dir="dwn">-</button>
                                        </span>
                                        <input type="text" class="number" value="1">
                                        <span class="input-group-btn plus">
                                            <button data-dir="up">+</button>
                                        </span>
                                    </div>
                                </div>  
                            </li>
                            <li>
                                <p style="font-size: 20px;"><span class="child-title">ĐƠN GIÁ:</span> 300.000 VNĐ</p>
                            </li>
                            <li>
                                <p style="font-size: 20px;"><span class="child-title">THÀNH TIỀN:</span> 300.000 VNĐ</p>
                            </li>
                        </ul>
                    </div>

                    <p class="sum">Tổng tiền: <span>600.000 VNĐ</span></p>
                    <a href="<?php echo site_url('pages/checkout.php');?>" class="button" title="">GỬI ĐƠN HÀNG</a>
                    <a href="" class="button" title="">TIẾP TỤC MUA</a>
                    <a href="" class="button" title="">CẬP NHẬT</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require '../footer.php';