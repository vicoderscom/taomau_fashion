	<div class="footer">
		<div class="container">
				<div class="logo-footer col-md-2 pad-l-0">
					<img src="../assets/images/footer/footer.png" alt="">
				</div>
				<div class="infomations col-md-10">
					<div class="menu-footer col-md-12 pad-0">
						<ul class="mar-0">
							<li><a href="" title="">Trang chủ</a></li>
							<li><a href="" title="">Giới thiệu</a></li>
							<li><a href="" title="">Blog</a></li>
							<li><a href="" title="">Shop</a></li>
							<li><a href="" title="">Sản phẩm</a></li>
							<li><a href="" title="">Liên hệ</a></li>
						</ul>
					</div>
					<div class="info col-md-12 pad-0">
						<div class="support col-md-3 pad-l-0">
							<p><a href="" title="">Hỗ trợ mua hàng</a></p>
							<p><a href="" title="">Hướng dẫn mua hàng</a></p>
							<p><a href="" title="">Hỏi đáp</a></p>
							<p><a href="" title="">Đổi trả hàng</a></p>
						</div>
						<div class="stores col-md-6 pad-l-0">
							<p>Hệ thống cửa hàng toàn quốc của WTW</p>
							<p>Miền Bắc:155 An Trạch – Cát Linh – Đống Đa – Hà Nội</p>
							<p>Điện thoại: 0163.222.6666</p>
							<p>Miền Trung – Nam: Lô 7 + 8. Đường Nguyễn tất Thành. </p>
							<p>P. Thanh Khê Tây. Quận Thanh Khê. Tp Đà Nẵng</p>
						</div>
						<div class="phone-email-follow col-md-3 pad-l-0 pad-r-0">
							<p>Điện thoại: 0988.999.679</p>
							<p>Email: Vpsports.jsc@gmail.com</p>
							<p>Follow us on&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								<a href="" title=""><img src="../assets/images/footer/fb.png" alt=""></a>
								<a href="" title=""><img src="../assets/images/footer/insta.png" alt=""></a>
								<a href="" title=""><img src="../assets/images/footer/g+.png" alt=""></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
