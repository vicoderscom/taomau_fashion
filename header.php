<!DOCTYPE html>
<html>
	<head>
		<title>What is wear</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<?php 
		require('init.php'); 
		?>

		<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/dist/app.css'); ?>">
		<script type="text/javascript" src="<?php echo site_url('assets/dist/app.js'); ?>"></script>
		
	</head>
	<body>
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="logo-wrapper clearfix">
							<a href="<?php echo site_url(); ?>"><img class="img-responsive logo-image" src="<?php echo site_url('assets/images/fashion-logo.png'); ?>"></a>
						</div>
					</div>
					<div class="col-md-10">
						<div class="navbar-header clearfix">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
							<ul class="menu-list menu-mobile">
								<li class="menu-item icon">
									<a href="#"><i class="fa fa-search"></i></a>
								</li>
								<li class="menu-item icon shopping-cart">
									<a href="<?php echo site_url('pages/cart.php'); ?>"><i class="fa fa-shopping-cart"></i></a>
									<span class="shopping-cart-notification">2</span>
								</li>
							</ul>
						</div>
						<div class="menu-wrapper collapse navbar-collapse" id="navbar-collapse">
							<ul class="menu-list">
								<li class="menu-item"><a href="<?php echo site_url();?>">TRANG CHỦ</a></li>
								<li class="menu-item"><a href="#">GIỚI THIỆU</a></li>
								<li class="menu-item"><a href="<?php echo site_url('pages/blog.php');?>">BLOG</a></li>
								<li class="menu-item"><a href="<?php echo site_url('pages/listproducts.php');?>">SẢN PHẨM</a></li>
								<li class="menu-item"><a href="<?php echo site_url('pages/contact.php');?>">LIÊN HỆ</a></li>
								<li class="menu-item icon">
									<a href="#"><i class="fa fa-search"></i></a>
								</li>
								<li class="menu-item icon shopping-cart">
									<a href="<?php echo site_url('pages/cart.php');?>"><i class="fa fa-shopping-cart"></i></a>
									<span class="shopping-cart-notification">2</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>