$(document).ready(function() {
	
     $(".listproducts_wrapper #slider-range").slider({
         range: true,
         min: 0,
         max: 100000,
         values: [50, 50000],
         slide: function(event, ui) {
             $(".listproducts_wrapper #amount").val("$" + ui.values[0] + " - " + ui.values[1] + " + ");
         }
     });
     $(".listproducts_wrapper #amount").val("$" + $(".listproducts_wrapper #slider-range").slider("values", 0) +
         " - " + $(".listproducts_wrapper #slider-range").slider("values", 1) +
         " + ");

});