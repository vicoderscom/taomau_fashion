$(document).ready(function() {
    function slickSetting() {
        var screen = {
            slidesToShow: 2,
            dots: true,
            arrows: false,
            autoplay: true
        };

        if (window.matchMedia('(max-width:768px)').matches) {
            screen.slidesToShow = 1;
        }

        return screen;
    }

    $('.blog-page .blog-top-slider').slick(slickSetting());
});