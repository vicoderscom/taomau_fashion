import 'bootstrap';
import 'jquery';
import 'jquery-ui-slider/jquery-ui.min.js';
import 'slick-carousel/slick/slick.min.js';

import './imports/_listproducts.js';
import './imports/_detailproduct.js';
import './imports/_cart.js';
import './imports/_blog-top-slider.js';
import './imports/number.js';

//cart-number				
$('.cart_wrapper input').each(function () {
	$(this).number();
});

$(document).on('click', '.number-spinner button', function() {
	var btn = $(this),
	oldValue = btn.closest('.number-spinner').find('input').val().trim(),
	newVal = 0;

	if (btn.attr('data-dir') == 'up') {
		newVal = parseInt(oldValue) + 1;
	} else {
		if (oldValue > 1) {
			newVal = parseInt(oldValue) - 1;
		} else {
			newVal = 1;
		}
	}
	btn.closest('.number-spinner').find('input').val(newVal);
});

// $(document).ready(function() {
// 	$('.related-products-section').slick({
// 		dots: false,
// 		infinite: false,
// 		speed: 300,
// 		slidesToShow: 4,
// 		slidesToScroll: 2,
// 		arrows:true,
// 		prevArrow: '<i class="fa fa-chevron-left" id ="icon-prev" aria-hidden="true"></i>',
// 		nextArrow: '<i class="fa fa-chevron-right" id ="icon-next" aria-hidden="true"></i>',
// 		responsive: [
// 			{
// 				breakpoint: 1024,
// 				settings: {
// 					slidesToShow: 2,
// 					slidesToScroll: 2,
// 					infinite: true,
// 					// dots: true
// 				}
// 			},
// 			{
// 				breakpoint: 600,
// 				settings: {
// 					slidesToShow: 2,
// 					slidesToScroll: 2
// 				}
// 			},
// 			{
// 				breakpoint: 480,
// 				settings: {
// 					slidesToShow: 1,
// 					slidesToScroll: 1
// 				}
// 			}
// 		]
// 	});
// });