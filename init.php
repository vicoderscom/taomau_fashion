<?php 

function site_url($uri = '') {
    $site_url = "http://localhost:8080/taomau_fashion/";
    if($uri != '') {
        $site_url .= '/'. $uri;
    }
    return $site_url;
}
 
function asset($assets)
{
    return site_url() . '/assets/dist/' . $assets;
}