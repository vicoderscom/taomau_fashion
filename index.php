<?php require 'header.php';?>
<div class="home-wrapper">
	<div class="banner-section">
		<img src="<?php echo site_url('assets/images/index/mask-banner.png'); ?>" alt="" style="background-image: url(<?php echo site_url('assets/images/index/banner-index.png'); ?>)">
	</div>

	<div class="menu-section">
		<div class="container">
			<div class="row">
				<div class="menu-left col-xs-12 col-sm-12 col-md-8 ">
					<div class="jacket-denim-tshirt col-md-12" style="background-image: url(<?php echo site_url('assets/images/index/bg-1.png'); ?>)">
						<p class="mar-0">
							<a href="" title="">Jacket</a><br>
							<a href="" title="">Denim</a><br>
							<a href="" title="">Tshirt</a><br>
						</p>
					</div>
					<div class="col-md-6 j1 pad-l-0">
						<div class="jogger-pants col-md-12 "  style="background-image: url(<?php echo site_url('assets/images/index/bg-jogger-pants.png'); ?>)">
							<p class="mar-0">
								<a href="" title="">Jogger</a><br>
								<a href="" title="">Pants</a><br>
							</p>
						</div>

					</div>
					<div class="col-md-6 j2 pad-r-0" >
						<div class="sneaker col-md-12" style="background-image: url(<?php echo site_url('assets/images/index/bg-sneaker.png'); ?>)">
							<p class="mar-0">
								<a href="" title="">Sneaker</a><br>
							</p>
						</div>
					</div>
				</div>
				<div class="menu-right col-xs-12 col-sm-12 col-md-4" >
					<div class="stylistic col-md-12" style="background-image: url(<?php echo site_url('assets/images/index/bg-2.png'); ?>)">
						<p class="mar-0">
							<a href="" title="">Stylistic</a><br>
						</p>
					</div>

				</div>
			</div>
		</div>
	</div>


	<div class="featured-products-section">
		<div class="container">
			<div class="row">
				<h1 class="mar-0">Sản phẩm nổi bật</h1>
				<?php for ($i = 0; $i < 4; $i++) {?>
					<div class="product col-xs-12 col-sm-6 col-md-3">
						<?php require 'pages/product.php';?>
					</div>
				<?php }?>

				<div class="paginate col-xs-12 col-md-12 col-md-12">
					<ul>
						<li><a class="fa fa-long-arrow-left" href="" title=""></a></li>
						<li><a class="active" href="" title="">1</a></li>
						<li><a href="" title="">2</a></li>
						<li><a href="" title="">3</a></li>
						<li><a class="fa fa-long-arrow-right" href="" title=""></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="sale-section" style="background-image: url(<?php echo site_url('assets/images/index/bg-sale.png'); ?>)">
		<div class="container">
			<div class="row">
				<div class="discount col-md-5 col-md-offset-1">
					<h1>Big Sale <br>
						DISCOUNT <br>
						UP TO 50%
					</h1>
					<p class="content">Lorem ipsum color sit amet, consectetuer adipiscing elit. Donec odio. Quisque volupat mattis ores. Nullam malesuada erat ut turpis. Surpendisse urna nibh, viverran non, semper suscipit, posuere a, pede.</p>
					<p class="time">
						<a href="" title="">1 Days</a>|
						<a href="" title="18 Hours">18 Hours</a>|
						<a href="" title="">30 Mins</a>|
						<a href="" title="">50 Secs</a>
					</p>
					<a href="" class="shop-now" title="">Shop now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="selling-prod-section">
		<div class="container">
			<div class="row">
				<div class="selling-top col-md-12">
					<div class="title col-md-4 pad-0">
						<h1 class="mar-0">Sản phẩm bán chạy</h1>
					</div>
					<div class="categories col-md-8 pad-0">
						<label for="">Tìm kiếm</label>
						<select name="">
							<option value="">Áo jacket</option>
							<option value="">Áo jacket 1</option>
							<option value="">Áo jacket 2</option>
						</select>
					</div>
				</div>
				<div class="products col-md-12 pad-0">
					<?php for ($i = 0; $i < 8; $i++) {?>
						<div class="product col-xs-12 col-sm-6 col-md-3">
							<?php require 'pages/product.php';?>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>

	<div class="fashion-blog-section">
		<div class="container">
			<div class="row">
				<h1 class="mar-0">fashion blog</h1>
				<div class="blog col-xs-12 col-sm-12 col-md-6">
					<div class="image col-xs-12 col-sm-6  col-md-6 pad-0">
						<a href="" title=""><img class="img-responsive" src="<?php echo site_url('assets/images/index/mask-blog.png'); ?>" style="background-image: url(<?php echo site_url('assets/images/index/blog2.png'); ?>)" alt=""></a>
					</div>
					<div class="blog-body col-xs-12 col-sm-6  col-md-6 pad-0">
						<a href="" title=""><p class="title">làm sao để kết hợp những phụ kiện thêm nổi bật?</p></a>
						<p class="date">20-08-2018</p>
						<p class="content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
					</div>
				</div>
				<div class="blog col-xs-12 col-sm-12 col-md-6">
					<div class="image col-xs-12 col-sm-6 col-md-6 pad-0">
						<a href="" title=""><img  class="img-responsive" src="<?php echo site_url('assets/images/index/mask-blog.png'); ?>" style="background-image: url(<?php echo site_url('assets/images/index/blog1.png'); ?>)" alt=""></a>
					</div>
					<div class="blog-body col-xs-12 col-sm-6 col-md-6 pad-0">
						<a href="" title=""><p class="title">làm sao để kết hợp những phụ kiện thêm nổi bật?</p></a>
						<p class="date">20-08-2018</p>
						<p class="content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="banner-footer-section" style="background-image: url(<?php echo site_url('assets/images/index/bannerfoot.png'); ?>)">
		<p>
			<a href="" title="">What the People are saying?</a>
		</p>
	</div>

	<div class="container">
		<div class="row">
			<div class="register">
				<div class="col-lg-6 col-md-6">
					<div class="register_mail">
						<p class="p1">Đăng ký Email</p>
						<p>Những tin tức, phiếu mua hàng và thông tin mới nhất</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form_mail">
						<input type="text" name="" placeholder="Nhập email của bạn">
						<button><i class="fa fa-envelope" aria-hidden="true"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require 'footer.php';?>